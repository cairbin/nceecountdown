# nceecountdown

#### 介绍
一个高考倒计时页面

作者：[CairBin](https://cairbin.top)

#### 演示地址
演示地址：https://cairbin.gitee.io/nceecountdown

#### 2020-07-19 更新
倒计时为0后自动切换至下一年
